use axum_extra::extract::{CookieJar, cookie::Cookie};
use serde::{Deserialize, Serialize};

use crate::{
    core::data_access::{TIMEZONES, get_and_cookie, patch_and_cookie, post_and_cookie},
    errors::CustomError,
};

#[derive(Deserialize)]
pub struct Timezone {
    pub name: String,
    pub minutes_offset: i32,
    pub selected: bool,
}

impl Timezone {
    pub async fn timezones_from_api(
        cookies: &CookieJar,
    ) -> Result<(Vec<Timezone>, Option<Cookie<'static>>), CustomError> {
        let (response, cookie) = get_and_cookie(TIMEZONES, cookies).await?;
        let timezones = response.json::<Vec<Timezone>>().await.unwrap();
        Ok((timezones, cookie))
    }

    pub async fn insert(
        cookies: &CookieJar,
        timezone: TimezoneForCreation,
    ) -> Result<Option<Cookie<'static>>, CustomError> {
        let (_, cookie) = post_and_cookie(TIMEZONES, cookies, timezone).await?;
        Ok(cookie)
    }
    pub async fn select(
        cookies: &CookieJar,
        timezone: String,
    ) -> Result<Option<Cookie<'static>>, CustomError> {
        let (_, cookie) = match timezone {
            s if s.is_empty() => return Err(CustomError::Forbidden),
            s => {
                let timezone_for_selection = TimezoneForSelection { selected: true };
                patch_and_cookie(
                    &format!("{}/{}", TIMEZONES, s),
                    cookies,
                    timezone_for_selection,
                )
                .await?
            }
        };
        Ok(cookie)
    }
}

#[derive(Serialize)]
pub struct TimezoneForCreation {
    pub name: String,
    pub minutes_offset: i32,
}

#[derive(Serialize)]
pub struct TimezoneForSelection {
    pub selected: bool,
}
