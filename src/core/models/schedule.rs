use std::collections::HashMap;

use axum_extra::extract::{CookieJar, cookie::Cookie};
use serde::{Deserialize, Serialize};
use thiserror::Error;

use crate::{
    core::data_access::{
        ApiError, TEMPERATURE_SCHEDULES, delete_and_cookie, get_and_cookie, patch_and_cookie,
        post_and_cookie,
    },
    errors::CustomError,
};

#[derive(Error, Debug)]
#[error(transparent)]
pub struct ScheduleError(#[from] ApiError);

pub struct Schedule {
    pub id: u8,
    pub name: String,
    pub schedule: HashMap<i32, f32>,
}

#[derive(Deserialize)]
pub struct ScheduleApiFormat {
    pub id: u8,
    pub name: String,
    pub schedule: Option<String>,
}

impl From<ScheduleApiFormat> for Schedule {
    fn from(value: ScheduleApiFormat) -> Self {
        let schedule = serde_json::from_str(&value.schedule.unwrap_or_default()).unwrap();
        Self {
            id: value.id,
            name: value.name,
            schedule,
        }
    }
}

impl From<Schedule> for ScheduleApiFormat {
    fn from(value: Schedule) -> Self {
        let schedule = serde_json::to_string(&value.schedule).unwrap();
        Self {
            id: value.id,
            name: value.name,
            schedule: Some(schedule),
        }
    }
}

#[derive(Serialize)]
pub struct ScheduleName {
    name: String,
}

#[derive(Serialize)]
pub struct ScheduleSchedule {
    schedule: String,
}

#[derive(Deserialize)]
pub struct ScheduleId {
    pub id: u8,
}

impl Schedule {
    pub async fn schedules_from_api(
        cookies: &CookieJar,
    ) -> (Option<Vec<Schedule>>, Option<Cookie<'static>>) {
        if cookies.get("refresh").is_none() {
            return (None, None);
        }

        let response;
        let optn_cookie;

        (response, optn_cookie) = match get_and_cookie(TEMPERATURE_SCHEDULES, cookies).await {
            Ok((r, o)) => (r, o),
            Err(_) => {
                println!("Error fetching valves data");
                return (None, None);
            }
        };

        if response.status().is_success() {
            let schedules = Some(
                response
                    .json::<Vec<ScheduleApiFormat>>()
                    .await
                    .unwrap()
                    .into_iter()
                    .map(|s| s.into())
                    .collect(),
            );
            (schedules, optn_cookie)
        } else {
            (None, optn_cookie)
        }
    }

    pub async fn delete_schedule(
        cookies: &CookieJar,
        id: u8,
    ) -> Result<Option<Cookie<'static>>, CustomError> {
        let (_, cookie) = delete_and_cookie(
            format!("{}/{}", TEMPERATURE_SCHEDULES, id).as_str(),
            cookies,
        )
        .await?;
        Ok(cookie)
    }

    pub async fn create_schedule(
        cookies: &CookieJar,
        name: String,
    ) -> Result<Option<Cookie<'static>>, CustomError> {
        let (_, cookie) = post_and_cookie(
            &format!("{}/{}", TEMPERATURE_SCHEDULES, "new"),
            cookies,
            ScheduleName { name },
        )
        .await?;
        Ok(cookie)
    }

    pub async fn update_name(
        cookies: &CookieJar,
        id: u8,
        name: String,
    ) -> Result<Option<Cookie<'static>>, CustomError> {
        let (_, cookie) = patch_and_cookie(
            &format!("{}/{}", TEMPERATURE_SCHEDULES, id),
            cookies,
            ScheduleName { name },
        )
        .await?;
        Ok(cookie)
    }

    pub async fn update_schedule(
        cookies: &CookieJar,
        id: u8,
        schedule: String,
    ) -> Result<Option<Cookie<'static>>, CustomError> {
        let (_, cookie) = patch_and_cookie(
            &format!("{}/{}", TEMPERATURE_SCHEDULES, id),
            cookies,
            ScheduleSchedule { schedule },
        )
        .await?;
        Ok(cookie)
    }

    pub async fn from_api(
        cookies: &CookieJar,
        id: u8,
    ) -> Result<(Schedule, Option<Cookie<'static>>), CustomError> {
        let (response, cookie) =
            get_and_cookie(&format!("{}/{}", TEMPERATURE_SCHEDULES, id), cookies).await?;
        let schedule = response.json::<ScheduleApiFormat>().await.unwrap().into();
        Ok((schedule, cookie))
    }
}
