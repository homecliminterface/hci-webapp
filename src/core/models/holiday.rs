use axum_extra::extract::{CookieJar, cookie::Cookie};
use serde::{Deserialize, Serialize};

use crate::core::{
    data_access::{HOLIDAY, delete_and_cookie, get_and_cookie, post_and_cookie},
    helpers::TimeOfWeek,
};

#[derive(Default)]
pub struct Holiday {
    pub day_of_week: i32,
    pub hour: u32,
    pub minute: u32,
    pub temperature: f32,
    pub active: bool,
}

impl From<HolidayApiFormat> for Holiday {
    fn from(value: HolidayApiFormat) -> Self {
        let time_of_week = TimeOfWeek::from(value.end_minutes);
        let day_of_week = time_of_week.day.into();
        let hour = time_of_week.hour;
        let minute = time_of_week.minute;
        let temperature = value.temperature;
        let active = value.active;
        Self {
            day_of_week,
            hour,
            minute,
            temperature,
            active,
        }
    }
}

impl Holiday {
    pub async fn from_api(cookies: &CookieJar) -> (Option<Holiday>, Option<Cookie<'static>>) {
        if cookies.get("refresh").is_none() {
            return (None, None);
        }

        let response;
        let optn_cookie;

        (response, optn_cookie) = match get_and_cookie(HOLIDAY, cookies).await {
            Ok((r, o)) => (r, o),
            Err(_) => {
                println!("Error fetching holiday data");
                return (None, None);
            }
        };

        if response.status().is_success() {
            let holiday = Some(response.json::<HolidayApiFormat>().await.unwrap().into());
            (holiday, optn_cookie)
        } else {
            (None, optn_cookie)
        }
    }

    pub async fn activate(
        cookies: &CookieJar,
        end_minutes: i32,
        temperature: f32,
    ) -> Option<Cookie<'static>> {
        match post_and_cookie(
            HOLIDAY,
            cookies,
            HolidayApiPost {
                end_minutes,
                temperature,
            },
        )
        .await
        {
            Ok((_, o)) => o,
            Err(err) => {
                log::error!("Error activating holiday: {:?}", err);
                None
            }
        }
    }

    pub async fn deactivate(cookies: &CookieJar) -> Option<Cookie<'static>> {
        match delete_and_cookie(HOLIDAY, cookies).await {
            Ok((_, o)) => o,
            Err(err) => {
                log::error!("Error deactivating holiday: {:?}", err);
                None
            }
        }
    }
}

#[derive(Deserialize)]
pub struct HolidayApiFormat {
    pub end_minutes: i32,
    pub temperature: f32,
    pub active: bool,
}

#[derive(Serialize)]
pub struct HolidayApiPost {
    pub end_minutes: i32,
    pub temperature: f32,
}
