use axum_extra::extract::{CookieJar, cookie::Cookie};
use serde::{Deserialize, Serialize};

use crate::core::data_access::{
    delete_and_cookie, get_and_cookie, post_and_cookie, put_and_cookie,
};

#[derive(Deserialize)]
pub struct Temperature {
    pub id: u8,
    pub name: String,
    pub letter_code: char,
    pub temperature: f32,
}

#[derive(Serialize)]
pub struct TemperatureTemperature {
    pub temperature: f32,
}

#[derive(Serialize)]
pub struct TemperatureToCreate {
    pub name: String,
    pub letter_code: char,
    pub temperature: f32,
}

impl Temperature {
    pub async fn temperatures_from_api(
        cookies: &CookieJar,
    ) -> (Option<Vec<Temperature>>, Option<Cookie<'static>>) {
        if cookies.get("refresh").is_none() {
            return (None, None);
        }

        let response;
        let optn_cookie;

        (response, optn_cookie) = match get_and_cookie(
            "https://hci.roland.tardieu.net/api/defaulttemperatures",
            cookies,
        )
        .await
        {
            Ok((r, o)) => (r, o),
            Err(_) => {
                println!("Error fetching preset temperatures data");
                return (None, None);
            }
        };

        if response.status().is_success() {
            (Some(response.json().await.unwrap()), optn_cookie)
        } else {
            (None, optn_cookie)
        }
    }

    pub async fn delete_temperature(cookies: &CookieJar, id: u8) -> Option<Cookie<'static>> {
        match delete_and_cookie(
            format!(
                "https://hci.roland.tardieu.net/api/defaulttemperatures/{}/",
                id
            )
            .as_str(),
            cookies,
        )
        .await
        {
            Ok((_, optn_cookie)) => optn_cookie,
            Err(_) => {
                println!("Impossible de supprimer la température");
                None
            }
        }
    }

    pub async fn update_temperature(
        cookies: &CookieJar,
        id: u8,
        temperature: f32,
    ) -> Option<Cookie<'static>> {
        match put_and_cookie(
            format!(
                "https://hci.roland.tardieu.net/api/defaulttemperatures/{}/update_temperature/",
                id
            )
            .as_str(),
            cookies,
            TemperatureTemperature { temperature },
        )
        .await
        {
            Ok((_, o)) => o,
            Err(_) => {
                println!("Error updating temperature");
                None
            }
        }
    }

    pub async fn create_temperature(
        cookies: &CookieJar,
        preset_temperature: TemperatureToCreate,
    ) -> Option<Cookie<'static>> {
        match post_and_cookie(
            "https://hci.roland.tardieu.net/api/defaulttemperatures/",
            cookies,
            preset_temperature,
        )
        .await
        {
            Ok((_, o)) => o,
            Err(_) => {
                println!("Error updating temperature");
                None
            }
        }
    }
}
