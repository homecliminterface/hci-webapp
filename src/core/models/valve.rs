use axum_extra::extract::{CookieJar, cookie::Cookie};
use serde::{Deserialize, Serialize};

use crate::core::data_access::{
    HEATER_VALVES, HV_UPDATE_SCHEDULE, HV_WANTED_TEMPERATURE, delete_and_cookie, get_and_cookie,
    put_and_cookie,
};

#[derive(Deserialize)]
pub struct Valve {
    pub friendly_name: String,
    pub measured_temperature: f32,
    pub wanted_temperature: f32,
    pub heat_required: bool,
    pub heating_demand: u32,
    pub schedule_name: Option<String>,
    pub schedule_id: Option<u8>,
}

#[derive(Serialize)]
pub struct ValveScheduleId {
    schedule_id: u8,
}

#[derive(Serialize)]
pub struct ValveWantedTemperature {
    wanted_temperature: f32,
    aggressive: bool,
}

impl Valve {
    pub async fn valves_from_api(
        cookies: &CookieJar,
    ) -> (Option<Vec<Valve>>, Option<Cookie<'static>>) {
        if cookies.get("refresh").is_none() {
            return (None, None);
        }

        let response;
        let optn_cookie;

        (response, optn_cookie) = match get_and_cookie(HEATER_VALVES, cookies).await {
            Ok((r, o)) => (r, o),
            Err(_) => {
                println!("Error fetching valves data");
                return (None, None);
            }
        };

        if response.status().is_success() {
            (Some(response.json().await.unwrap()), optn_cookie)
        } else {
            (None, optn_cookie)
        }
    }

    pub async fn set_schedule(
        cookies: &CookieJar,
        valve_id: &str,
        schedule: Option<u8>,
    ) -> Option<Cookie<'static>> {
        if let Some(schedule) = schedule {
            match put_and_cookie(
                format!("{}/{}/{}", HEATER_VALVES, valve_id, HV_UPDATE_SCHEDULE).as_str(),
                cookies,
                ValveScheduleId {
                    schedule_id: schedule,
                },
            )
            .await
            {
                Ok((_, o)) => o,
                Err(err) => {
                    log::error!("Error setting schedule for valve: {:?}", err);
                    None
                }
            }
        } else {
            match delete_and_cookie(
                format!("{}/{}/{}", HEATER_VALVES, valve_id, HV_UPDATE_SCHEDULE).as_str(),
                cookies,
            )
            .await
            {
                Ok((_, o)) => o,
                Err(_) => {
                    println!("Error deleting schedule for valve");
                    None
                }
            }
        }
    }

    pub async fn set_temperature(
        cookies: &CookieJar,
        valve_id: &str,
        wanted_temperature: f32,
        aggressive: bool,
    ) -> Option<Cookie<'static>> {
        match put_and_cookie(
            format!("{}/{}/{}", HEATER_VALVES, valve_id, HV_WANTED_TEMPERATURE).as_str(),
            cookies,
            ValveWantedTemperature {
                wanted_temperature,
                aggressive,
            },
        )
        .await
        {
            Ok((_, o)) => o,
            Err(_) => {
                println!("Error setting wanted_temperature for valve");
                None
            }
        }
    }
}
