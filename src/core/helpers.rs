use DayOfWeek::*;
use std::fmt::Display;

pub struct TimeOfWeek {
    pub day: DayOfWeek,
    pub hour: u32,
    pub minute: u32,
}

impl Display for TimeOfWeek {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}, {}h{}", self.day, self.hour, self.minute)
    }
}

impl From<i32> for TimeOfWeek {
    fn from(value: i32) -> Self {
        let value = value % 10080;
        let day_number = value / 1440;
        let minutes = value - day_number * 1440;
        let hour = minutes / 60;
        let minute = minutes % 60;
        Self {
            day: day_number.try_into().unwrap(),
            hour: hour.try_into().unwrap(),
            minute: minute.try_into().unwrap(),
        }
    }
}

pub enum DayOfWeek {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday,
}

impl TryFrom<i32> for DayOfWeek {
    type Error = String;

    fn try_from(value: i32) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Monday),
            1 => Ok(Tuesday),
            2 => Ok(Wednesday),
            3 => Ok(Thursday),
            4 => Ok(Friday),
            5 => Ok(Saturday),
            6 => Ok(Sunday),
            i32::MIN..=-1_i32 | 7_i32..=i32::MAX => Err("Invalid day number".into()),
        }
    }
}

impl Display for DayOfWeek {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Monday => "lundi",
                Tuesday => "mardi",
                Wednesday => "mercredi",
                Thursday => "jeudi",
                Friday => "vendredi",
                Saturday => "samedi",
                Sunday => "dimanche",
            }
        )
    }
}

impl From<DayOfWeek> for i32 {
    fn from(value: DayOfWeek) -> Self {
        match value {
            Monday => 0,
            Tuesday => 1,
            Wednesday => 2,
            Thursday => 3,
            Friday => 4,
            Saturday => 5,
            Sunday => 6,
        }
    }
}

impl From<TimeOfWeek> for i32 {
    fn from(value: TimeOfWeek) -> Self {
        i32::from(value.day) * 24 * 60 + (value.hour as i32) * 60 + (value.minute as i32)
    }
}
