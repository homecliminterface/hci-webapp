use axum_extra::extract::{CookieJar, cookie::Cookie};
use const_format::concatcp;
use once_cell::sync::OnceCell;
use reqwest::{Client, Response, StatusCode};
use serde::{Deserialize, Serialize};
use thiserror::Error;

use crate::http::templates::login::LoginForm;

const URL_API: &str = "https://hci.roland.tardieu.net";

pub const TOKEN_REFRESH: &str = concatcp!(URL_API, "/token/refresh");
pub const TOKEN: &str = concatcp!(URL_API, "/token");

pub const HEATER_VALVES: &str = concatcp!(URL_API, "/heater_valve");

pub const HV_UPDATE_SCHEDULE: &str = "schedule";
pub const HV_WANTED_TEMPERATURE: &str = "wanted_temperature";

pub const TEMPERATURE_SCHEDULES: &str = concatcp!(URL_API, "/valve_schedule");

pub const HOLIDAY: &str = concatcp!(URL_API, "/holiday");

pub const TIMEZONES: &str = concatcp!(URL_API, "/timezone");

#[derive(Error, Debug)]
pub enum ApiError {
    #[error("Resource not found: {0}")]
    NotFound(String),
    #[error("Bad authentication: {0}")]
    Authentication(String),
    #[error(transparent)]
    Reqwest(#[from] reqwest::Error),
    #[error("Bad response: {0:?}")]
    BadResponse(Response),
}

#[derive(Debug)]
pub struct ApiTools {
    pub client: Client,
}
static INSTANCE: OnceCell<ApiTools> = OnceCell::new();
impl ApiTools {
    pub fn global() -> &'static Self {
        INSTANCE.get().expect("ApiTools is not initialized")
    }
    pub fn client() -> &'static Client {
        &INSTANCE.get().expect("ApiTools is not initialized").client
    }
}
pub fn initialize_api_tools() {
    let api_tools = ApiTools {
        client: Client::new(),
    };
    INSTANCE.set(api_tools).unwrap();
}

pub async fn get_and_cookie(
    url: &str,
    cookies: &CookieJar,
) -> Result<(Response, Option<Cookie<'static>>), ApiError> {
    request_and_cookie::<()>(&RequestType::Get, url, cookies, None).await
}

pub async fn put_and_cookie<T: Serialize>(
    url: &str,
    cookies: &CookieJar,
    content: T,
) -> Result<(Response, Option<Cookie<'static>>), ApiError> {
    request_and_cookie(&RequestType::Put, url, cookies, Some(content)).await
}

pub async fn delete_and_cookie(
    url: &str,
    cookies: &CookieJar,
) -> Result<(Response, Option<Cookie<'static>>), ApiError> {
    request_and_cookie::<()>(&RequestType::Delete, url, cookies, None).await
}

pub async fn post_and_cookie<T: Serialize>(
    url: &str,
    cookies: &CookieJar,
    content: T,
) -> Result<(Response, Option<Cookie<'static>>), ApiError> {
    request_and_cookie(&RequestType::Post, url, cookies, Some(content)).await
}

pub async fn patch_and_cookie<T: Serialize>(
    url: &str,
    cookies: &CookieJar,
    content: T,
) -> Result<(Response, Option<Cookie<'static>>), ApiError> {
    request_and_cookie(&RequestType::Patch, url, cookies, Some(content)).await
}

async fn request_and_cookie<T: Serialize>(
    request_type: &RequestType,
    url: &str,
    cookies: &CookieJar,
    content: Option<T>,
) -> Result<(Response, Option<Cookie<'static>>), ApiError> {
    let client = ApiTools::client();
    let mut access_token = AccessToken::from_cookies(&cookies).unwrap_or_default();
    let mut response = get_response(request_type, client, url, &access_token, &content).await?;

    let mut access_cookie = None;
    if response.status() == StatusCode::UNAUTHORIZED {
        access_token = refresh_access_token(&RefreshToken::from_cookies(&cookies)?).await?;
        response = get_response(request_type, client, url, &access_token, &content).await?;
        access_cookie = Some(Cookie::new("access", access_token.access));
    }

    if response.status().is_success() {
        Ok((response, access_cookie))
    } else {
        Err(ApiError::BadResponse(response))
    }
}

async fn get_response<T: Serialize>(
    request_type: &RequestType,
    client: &Client,
    url: &str,
    access_token: &AccessToken,
    content: &Option<T>,
) -> Result<Response, ApiError> {
    let mut request_builder = match request_type {
        RequestType::Get => client.get(url),
        RequestType::Post => client.post(url),
        RequestType::Put => client.put(url),
        RequestType::Patch => client.patch(url),
        RequestType::Delete => client.delete(url),
    };
    request_builder = request_builder.bearer_auth(&access_token.access);
    if let Some(content) = content {
        request_builder = request_builder.json(content);
    }
    let request = request_builder.build()?;
    Ok(client.execute(request).await?)
}

pub enum RequestType {
    Get,
    Post,
    Put,
    Patch,
    Delete,
}

pub async fn refresh_access_token(refresh_token: &RefreshToken) -> Result<AccessToken, ApiError> {
    let client = ApiTools::client();
    let request = client
        .post(TOKEN_REFRESH)
        .json(refresh_token)
        .build()
        .unwrap();
    let response = client.execute(request).await.unwrap();
    println!("{}", response.status());
    if response.status().is_success() {
        let access_token: AccessToken = response.json().await.unwrap();
        Ok(access_token)
    } else {
        println!("Error when refreshing access token");
        Err(ApiError::Authentication(
            "Error when refreshing access token".into(),
        ))
    }
}

pub async fn login(login_form: &LoginForm) -> Result<Credentials, ApiError> {
    let client = ApiTools::client();
    let request = client.post(TOKEN).json(login_form).build().unwrap();
    let response = client.execute(request).await.unwrap();
    if response.status().is_success() {
        let credentials: Credentials = response.json().await.unwrap();
        log::info!("Credentials obtained");
        return Ok(credentials);
    }
    log::error!(
        "API Error : Impossible to get credentials from API. Response: {:?}",
        response
    );
    Err(ApiError::Authentication(
        "Impossible to get credentials from API".into(),
    ))
}

#[derive(Serialize, Deserialize, Default)]
pub struct AccessToken {
    pub access: String,
}

impl AccessToken {
    pub fn from_cookies(cookies: &CookieJar) -> Result<Self, ApiError> {
        match cookies.get("access") {
            Some(cookie) => Ok(Self {
                access: cookie.value().to_string(),
            }),
            None => Err(ApiError::Authentication(
                "Impossible to get access token from cookies".into(),
            )),
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct RefreshToken {
    pub refresh: String,
}

impl RefreshToken {
    pub fn from_cookies(cookies: &CookieJar) -> Result<Self, ApiError> {
        match cookies.get("refresh") {
            Some(cookie) => Ok(Self {
                refresh: cookie.value().to_string(),
            }),
            None => Err(ApiError::Authentication(
                "impossible to get refresh token from cookies".into(),
            )),
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct Credentials {
    pub access: String,
    pub refresh: String,
}

#[derive(Serialize)]
pub struct ObjectId {
    id: u8,
}
