use axum::http::HeaderValue;
use axum::response::IntoResponse;
use reqwest::StatusCode;
use thiserror::Error;

use crate::core::data_access::ApiError;

#[derive(Error, Debug)]
pub enum CustomError {
    #[error(transparent)]
    ApiError(#[from] ApiError),
    #[error("Operation forbidden")]
    Forbidden,
}

impl IntoResponse for CustomError {
    fn into_response(self) -> axum::response::Response {
        match self {
            Self::ApiError(error) => {
                let mut response = format!("<h1>Erreur 500</h1><p>{}</p>", error).into_response();
                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                response
                    .headers_mut()
                    .insert("Content-Type", HeaderValue::from_str("html").unwrap());
                response
            }
            Self::Forbidden => {
                let mut response =
                    "<h1>Error 403: Forbidden</h1><p>The attempted operation is forbidden.</p>"
                        .into_response();
                *response.status_mut() = StatusCode::FORBIDDEN;
                response
                    .headers_mut()
                    .insert("Content-Type", HeaderValue::from_str("html").unwrap());
                response
            }
        }
    }
}
