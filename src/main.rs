use clap::{Command, crate_version};
use hci_webapp::core::data_access;
use hci_webapp::http;
use hci_webapp::http::urls;

#[tokio::main]
async fn main() {
    dotenvy::dotenv().ok();

    env_logger::init();

    let _ = cli_config().get_matches();

    urls::initialize_urls();
    data_access::initialize_api_tools();

    let host = dotenvy::var("HOST").unwrap();
    let port = dotenvy::var("PORT").unwrap();
    http::run_server(&host, &port).await;
}

fn cli_config() -> Command {
    Command::new("HomeClimInterface - Web interface")
        .version(crate_version!())
        .author("Roland Tardieu-Collinet <roland@tardieu.net>")
}
