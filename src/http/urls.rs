use std::collections::HashMap;

use once_cell::sync::OnceCell;

const URLS: [(&str, &str); 8] = [
    ("static", "/static"),
    ("heater_valves", "/"),
    ("temperature_schedules", "/schedules"),
    ("preset_temperatures", "/temperatures"),
    ("login", "/login"),
    ("logout", "/logout"),
    ("parameters", "/parameters"),
    ("holiday", "/holiday"),
];

#[derive(Debug)]
pub struct Urls {
    map: HashMap<&'static str, &'static str>,
}

static INSTANCE: OnceCell<Urls> = OnceCell::new();

impl Urls {
    pub fn global() -> &'static Self {
        INSTANCE.get().expect("Urls is not initialized")
    }

    pub fn get(name: &str) -> &'static str {
        let url = Self::global().map.get(name).expect("Page name is invalid");
        println!("Demande d'accès à l'URL : {}", url);
        url
    }

    pub fn get_all() -> &'static HashMap<&'static str, &'static str> {
        let urls = &Self::global().map;
        println!("Demande d'accès à la liste globale des URLs.");
        urls
    }

    pub fn from_const(data: [(&'static str, &'static str); 8]) -> Self {
        Self {
            map: HashMap::from(data),
        }
    }
}

pub fn initialize_urls() {
    let urls = Urls::from_const(URLS);
    INSTANCE.set(urls).unwrap();
}
