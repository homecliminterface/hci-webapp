use axum::body::Body;
use axum::http::{Request, Response, StatusCode, Uri};
use tower::ServiceExt;
use tower_http::services::ServeDir;
use tower_http::services::fs::ServeFileSystemResponseBody;

pub async fn file_handler(
    uri: Uri,
) -> Result<Response<ServeFileSystemResponseBody>, (StatusCode, String)> {
    let request = Request::builder().uri(uri).body(Body::empty()).unwrap();

    let static_dir = dotenvy::var("STATIC_DIR").unwrap();

    match ServeDir::new(&static_dir).oneshot(request).await {
        Ok(res) => Ok(res),
        Err(e) => Err((
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("Something went wrong: {}", e),
        )),
    }
}
