use std::collections::HashMap;

use askama::Template;
use axum::{
    Form,
    response::{IntoResponse, Redirect, Response},
};
use axum_extra::extract::CookieJar;
use serde::Deserialize;

use crate::{
    core::models::temperature::{Temperature, TemperatureToCreate},
    http::{askama_axum::response_from, urls::Urls},
};

#[derive(Template)]
#[template(path = "default_temperatures.html")]
pub struct DefaultTemperaturesTemplate {
    temperatures: Vec<Temperature>,
    urls: &'static HashMap<&'static str, &'static str>,
}

pub async fn default_temperatures_get(cookies: CookieJar) -> Response {
    let (optn_temperatures, optn_cookie) = Temperature::temperatures_from_api(&cookies).await;

    match optn_temperatures {
        None => Redirect::to(Urls::get("login")).into_response(),
        Some(temperatures) => {
            let template = DefaultTemperaturesTemplate {
                temperatures,
                urls: Urls::get_all(),
            };
            match optn_cookie {
                Some(c) => (CookieJar::new().add(c), response_from(&template)).into_response(),
                None => response_from(&template),
            }
        }
    }
}

pub async fn default_temperatures_post(
    cookies: CookieJar,
    Form(default_temperature_form): Form<DefaultTemperatureForm>,
) -> Response {
    let mut optn_cookie = None;
    match default_temperature_form.temperature_id {
        Some(id) => {
            if default_temperature_form.delete.is_some() {
                optn_cookie = Temperature::delete_temperature(&cookies, id.parse().unwrap()).await;
            }
            if default_temperature_form.update.is_some() {
                optn_cookie = Temperature::update_temperature(
                    &cookies,
                    id.parse().unwrap(),
                    default_temperature_form
                        .temperature
                        .unwrap()
                        .parse()
                        .unwrap(),
                )
                .await;
            }
        }
        None => {
            let preset_temperature = TemperatureToCreate {
                name: default_temperature_form.name.unwrap(),
                letter_code: default_temperature_form
                    .letter_code
                    .unwrap()
                    .parse()
                    .unwrap(),
                temperature: default_temperature_form
                    .temperature
                    .unwrap()
                    .parse()
                    .unwrap(),
            };
            optn_cookie = Temperature::create_temperature(&cookies, preset_temperature).await;
        }
    }
    let redirect = Redirect::to(Urls::get("preset_temperatures"));
    match optn_cookie {
        Some(c) => (CookieJar::new().add(c), redirect).into_response(),
        None => redirect.into_response(),
    }
}

#[derive(Deserialize)]
pub struct DefaultTemperatureForm {
    pub temperature_id: Option<String>,
    pub delete: Option<String>,
    pub update: Option<String>,
    pub name: Option<String>,
    pub letter_code: Option<String>,
    pub temperature: Option<String>,
}
