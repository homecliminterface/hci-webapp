use std::collections::HashMap;

use askama::Template;
use axum::{
    Form,
    response::{IntoResponse, Redirect, Response},
};
use axum_extra::extract::{CookieJar, cookie::Cookie};
use serde::{Deserialize, Serialize};

use crate::{
    core::data_access::login,
    http::{askama_axum::response_from, urls::Urls},
};

#[derive(Template)]
#[template(path = "login.html")]
pub struct LoginTemplate {
    urls: &'static HashMap<&'static str, &'static str>,
}

pub async fn login_get(cookies: CookieJar) -> Response {
    if cookies.get("refresh").is_some() {
        return Redirect::to(Urls::get("logout")).into_response();
    }
    response_from(&LoginTemplate {
        urls: Urls::get_all(),
    })
}

#[derive(Serialize, Deserialize)]
pub struct LoginForm {
    username: String,
    password: String,
}

pub async fn login_post(Form(login_form): Form<LoginForm>) -> Response {
    match login(&login_form).await {
        Ok(credentials) => {
            let mut cookies = CookieJar::new();
            cookies = cookies.add(Cookie::new("access", credentials.access));
            cookies = cookies.add(Cookie::new("refresh", credentials.refresh));
            cookies = cookies.add(Cookie::new("username", login_form.username));
            (cookies, Redirect::to(Urls::get("heater_valves"))).into_response()
        }
        Err(_) => Redirect::to(Urls::get("login")).into_response(),
    }
}
