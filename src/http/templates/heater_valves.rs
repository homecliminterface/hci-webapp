use std::collections::HashMap;

use askama::Template;
use axum::Form;
use axum::response::{IntoResponse, Redirect, Response};
use axum_extra::extract::CookieJar;
use serde::Deserialize;

use crate::core::models::holiday::Holiday;
use crate::core::models::schedule::Schedule;
use crate::core::models::valve::Valve;

use crate::core::helpers::{DayOfWeek, TimeOfWeek};

use crate::http::askama_axum::response_from;
use crate::http::urls::Urls;

#[derive(Template)]
#[template(path = "heater_valves.html")]
pub struct HeaterValvesTemplate {
    valves: Vec<Valve>,
    schedules: Vec<Schedule>,
    urls: &'static HashMap<&'static str, &'static str>,
    holiday: Holiday,
    temperatures: Vec<f32>,
    holiday_form_url: String,
}

pub async fn heater_valves_get(cookies: CookieJar) -> Response {
    let (optn_valves, _) = Valve::valves_from_api(&cookies).await;
    let (optn_schedules, _) = Schedule::schedules_from_api(&cookies).await;
    let (optn_holiday, optn_cookie) = Holiday::from_api(&cookies).await;
    if optn_valves.is_none() || optn_schedules.is_none() || optn_holiday.is_none() {
        return Redirect::to(Urls::get("login")).into_response();
    }
    let mut valves = optn_valves.unwrap();
    valves.sort_by(|a, b| a.friendly_name.cmp(&b.friendly_name));
    let temperatures = (0..60).map(|m| 5.0 + (m as f32) * 0.5).collect();
    let template = HeaterValvesTemplate {
        valves,
        schedules: optn_schedules.unwrap(),
        urls: Urls::get_all(),
        holiday: optn_holiday.unwrap(),
        temperatures,
        holiday_form_url: Urls::get("holiday").to_string(),
    };
    match optn_cookie {
        Some(c) => (CookieJar::new().add(c), response_from(&template)).into_response(),
        None => response_from(&template),
    }
}

pub async fn heater_valves_post(
    cookies: CookieJar,
    Form(heater_valve_form): Form<HeaterValveForm>,
) -> Response {
    let valve_id = heater_valve_form.valve_id;
    let optn_access;
    if heater_valve_form.unset_schedule.is_some() {
        optn_access = Valve::set_schedule(&cookies, &valve_id, None).await;
    } else if heater_valve_form.update_schedule.is_some() {
        let schedule: Option<u8> = heater_valve_form.schedule.map(|s| s.parse().unwrap());
        optn_access = Valve::set_schedule(&cookies, &valve_id, schedule).await;
    } else {
        match heater_valve_form.wanted_temperature.unwrap().parse::<f32>() {
            Ok(temperature) => {
                let aggressive = match heater_valve_form.aggressive {
                    Some(_) => true,
                    None => false,
                };
                optn_access =
                    Valve::set_temperature(&cookies, &valve_id, temperature, aggressive).await;
            }
            Err(_) => {
                println!("Error when getting temperature to set from HTML form");
                optn_access = None
            }
        };
    }
    let redirect = Redirect::to(Urls::get("heater_valves"));
    match optn_access {
        Some(c) => (CookieJar::new().add(c), redirect).into_response(),
        None => redirect.into_response(),
    }
}

pub async fn holiday_post(cookies: CookieJar, Form(holiday_form): Form<HolidayForm>) -> Response {
    let optn_access;
    if holiday_form.activate.is_some() {
        let time_of_week = TimeOfWeek {
            day: holiday_form.day_of_week.unwrap().try_into().unwrap(),
            hour: holiday_form.hour.unwrap(),
            minute: holiday_form.minute.unwrap(),
        };
        optn_access = Holiday::activate(
            &cookies,
            time_of_week.into(),
            holiday_form.temperature.unwrap(),
        )
        .await;
    } else {
        optn_access = Holiday::deactivate(&cookies).await;
    }

    let redirect = Redirect::to(Urls::get("heater_valves"));
    match optn_access {
        Some(c) => (CookieJar::new().add(c), redirect).into_response(),
        None => redirect.into_response(),
    }
}

#[derive(Deserialize)]
pub struct HeaterValveForm {
    pub valve_id: String,
    pub unset_schedule: Option<String>,
    pub schedule: Option<String>,
    pub update_schedule: Option<String>,
    pub wanted_temperature: Option<String>,
    pub update_temperature: Option<String>,
    pub aggressive: Option<String>,
}

#[derive(Deserialize)]
pub struct HolidayForm {
    pub day_of_week: Option<i32>,
    pub hour: Option<u32>,
    pub minute: Option<u32>,
    pub temperature: Option<f32>,
    pub deactivate: Option<String>,
    pub activate: Option<String>,
}
