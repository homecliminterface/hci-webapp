use std::collections::HashMap;

use askama::Template;
use axum::response::{IntoResponse, Redirect, Response};
use axum_extra::extract::{CookieJar, cookie::Cookie};

use crate::http::{askama_axum::response_from, urls::Urls};

#[derive(Template)]
#[template(path = "logout.html")]
struct LogoutTemplate {
    urls: &'static HashMap<&'static str, &'static str>,
    username: String,
}

pub async fn logout_get(cookies: CookieJar) -> Response {
    response_from(&LogoutTemplate {
        urls: Urls::get_all(),
        username: match cookies.get("username") {
            Some(u) => u.value().to_string(),
            None => String::new(),
        },
    })
}

pub async fn logout_post(mut cookies: CookieJar) -> Response {
    cookies = cookies.remove(Cookie::from("username"));
    cookies = cookies.remove(Cookie::from("access"));
    cookies = cookies.remove(Cookie::from("refresh"));
    (cookies, Redirect::to(Urls::get("login"))).into_response()
}
