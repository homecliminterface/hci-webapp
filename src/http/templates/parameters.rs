use std::collections::HashMap;

use askama::Template;
use axum::{
    Form,
    response::{IntoResponse, Redirect, Response},
};
use axum_extra::extract::CookieJar;
use serde::Deserialize;

use crate::{
    core::models::timezone::{Timezone, TimezoneForCreation},
    errors::CustomError,
    http::{askama_axum::response_from, urls::Urls},
};

#[derive(Template)]
#[template(path = "parameters.html")]
struct ParametersTemplate {
    pub urls: &'static HashMap<&'static str, &'static str>,
    pub timezones: Vec<Timezone>,
}

pub async fn parameters_get(cookies: CookieJar) -> Result<Response, CustomError> {
    let (timezones, cookie) = Timezone::timezones_from_api(&cookies).await?;
    let mut cookie_jar = CookieJar::new();
    if let Some(cookie) = cookie {
        cookie_jar = cookie_jar.add(cookie);
    }
    Ok((
        cookie_jar,
        response_from(&ParametersTemplate {
            urls: Urls::get_all(),
            timezones,
        }),
    )
        .into_response())
}

pub async fn parameters_post(
    cookies: CookieJar,
    Form(tzf): Form<TimezoneForm>,
) -> Result<Response, CustomError> {
    let cookie;
    if let Some(timezone) = tzf.timezone {
        cookie = Timezone::select(&cookies, timezone).await?;
    } else if let (Some(name), Some(minutes_offset)) = (tzf.name, tzf.minutes_offset) {
        let timezone_for_creation = TimezoneForCreation {
            name,
            minutes_offset,
        };
        cookie = Timezone::insert(&cookies, timezone_for_creation).await?;
    } else {
        return Err(CustomError::Forbidden);
    }
    let mut cookie_jar = CookieJar::new();
    if let Some(cookie) = cookie {
        cookie_jar = cookie_jar.add(cookie);
    }
    Ok((cookie_jar, Redirect::to(Urls::get("parameters"))).into_response())
}

#[derive(Deserialize)]
pub struct TimezoneForm {
    pub timezone: Option<String>,
    pub name: Option<String>,
    pub minutes_offset: Option<i32>,
}
