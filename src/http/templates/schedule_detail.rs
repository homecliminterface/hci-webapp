use std::collections::HashMap;

use crate::{core::helpers::TimeOfWeek, http::askama_axum::response_from};
use askama::Template;
use axum::{
    Form,
    extract::Path,
    response::{IntoResponse, Redirect, Response},
};
use axum_extra::extract::CookieJar;
use serde::Deserialize;

use crate::core::models::schedule::ScheduleApiFormat;
use crate::{
    core::{helpers::DayOfWeek, models::schedule::Schedule},
    errors::CustomError,
    http::urls::Urls,
};

#[derive(Template)]
#[template(path = "temperature_schedule_details.html")]
pub struct TemperatureScheduleDetailsTemplate {
    pub urls: &'static HashMap<&'static str, &'static str>,
    pub schedule: Schedule,
    pub schedule_schedule_sorted: Vec<(i32, f32)>,
    pub hours: Vec<i32>,
    pub minutes: Vec<i32>,
    pub temperatures: Vec<f32>,
}

pub async fn temperature_schedule_detail_get(
    cookies: CookieJar,
    Path(id): Path<u8>,
) -> Result<Response, CustomError> {
    let (schedule, cookie) = Schedule::from_api(&cookies, id).await?;
    let hours = (0..24).collect();
    let minutes = (0..12).map(|m| m * 5).collect();
    let temperatures = (0..60).map(|m| 5.0 + (m as f32) * 0.5).collect();
    let mut schedule_schedule_sorted: Vec<(i32, f32)> =
        schedule.schedule.clone().into_iter().collect();
    schedule_schedule_sorted.sort_by_key(|(a, _)| *a);
    let template = TemperatureScheduleDetailsTemplate {
        urls: Urls::get_all(),
        schedule,
        schedule_schedule_sorted,
        hours,
        minutes,
        temperatures,
    };
    Ok(if let Some(cookie) = cookie {
        (CookieJar::new().add(cookie), response_from(&template)).into_response()
    } else {
        response_from(&template)
    })
}

pub async fn temperature_schedule_detail_post(
    cookies: CookieJar,
    Path(id): Path<u8>,
    Form(sdf): Form<ScheduleDetailsForm>,
) -> Result<Response, CustomError> {
    if id != sdf.id {
        return Err(CustomError::Forbidden);
    }
    if let Some(name) = sdf.name {
        log::info!("Modifying schedule name");
        let cookie = Schedule::update_name(&cookies, id, name).await?;
        let mut cookie_jar = CookieJar::new();
        if let Some(cookie) = cookie {
            cookie_jar = cookie_jar.add(cookie);
        }
        Ok((
            cookie_jar,
            Redirect::to(&format!("{}/{}", Urls::get("temperature_schedules"), id)),
        )
            .into_response())
    } else if let (Some(day), Some(hour), Some(minute), Some(temperature)) =
        (sdf.day_of_week, sdf.hour, sdf.minute, sdf.temperature)
    {
        log::info!("Adding a schedule entry");
        let time_of_week = TimeOfWeek {
            day: day.try_into().unwrap(),
            hour,
            minute,
        };
        let (mut schedule, _) = Schedule::from_api(&cookies, id).await?;
        schedule.schedule.insert(time_of_week.into(), temperature);
        let cookie = Schedule::update_schedule(
            &cookies,
            id,
            ScheduleApiFormat::from(schedule).schedule.unwrap(),
        )
        .await?;
        let mut cookie_jar = CookieJar::new();
        if let Some(cookie) = cookie {
            cookie_jar = cookie_jar.add(cookie);
        }
        Ok((
            cookie_jar,
            Redirect::to(&format!("{}/{}", Urls::get("temperature_schedules"), id)),
        )
            .into_response())
    } else if let Some(minutes) = sdf.delete_entry {
        log::info!("Deleting a schedule entry");
        let (mut schedule, _) = Schedule::from_api(&cookies, id).await?;
        log::info!("Schedule before removal: {:?}", schedule.schedule);
        schedule.schedule.remove(&minutes);
        log::info!("Schedule after removal: {:?}", schedule.schedule);
        let cookie = Schedule::update_schedule(
            &cookies,
            id,
            ScheduleApiFormat::from(schedule).schedule.unwrap(),
        )
        .await?;
        let mut cookie_jar = CookieJar::new();
        if let Some(cookie) = cookie {
            cookie_jar = cookie_jar.add(cookie);
        }
        Ok((
            cookie_jar,
            Redirect::to(&format!("{}/{}", Urls::get("temperature_schedules"), id)),
        )
            .into_response())
    } else {
        log::error!("Action not understood");
        Err(CustomError::Forbidden)
    }
}

#[derive(Deserialize)]
pub struct ScheduleDetailsForm {
    pub id: u8,
    pub name: Option<String>,
    pub day_of_week: Option<i32>,
    pub hour: Option<u32>,
    pub minute: Option<u32>,
    pub temperature: Option<f32>,
    pub delete_entry: Option<i32>,
}
