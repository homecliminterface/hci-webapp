use std::collections::HashMap;

use askama::Template;
use axum::{
    Form,
    response::{IntoResponse, Redirect, Response},
};
use axum_extra::extract::CookieJar;
use serde::Deserialize;

use crate::{
    core::models::schedule::Schedule,
    errors::CustomError,
    http::{askama_axum::response_from, urls::Urls},
};

#[derive(Template)]
#[template(path = "temperature_schedules.html")]
pub struct TemperatureSchedulesTemplate {
    pub urls: &'static HashMap<&'static str, &'static str>,
    pub schedules: Vec<Schedule>,
}

#[derive(Deserialize)]
pub struct TemperatureSchedulesForm {
    pub delete_schedule: Option<String>,
    pub schedule_name: Option<String>,
    pub create_schedule: Option<String>,
}

pub async fn temperature_schedules_get(cookies: CookieJar) -> Response {
    let (schedules, optn_cookie) = match Schedule::schedules_from_api(&cookies).await {
        (Some(l), oc) => (l, oc),
        (None, _) => return Redirect::to(Urls::get("login")).into_response(),
    };
    let template = TemperatureSchedulesTemplate {
        urls: Urls::get_all(),
        schedules,
    };
    match optn_cookie {
        Some(c) => (CookieJar::new().add(c), response_from(&template)).into_response(),
        None => response_from(&template),
    }
}

pub async fn temperature_schedules_post(
    cookies: CookieJar,
    Form(schedules_form): Form<TemperatureSchedulesForm>,
) -> Result<Response, CustomError> {
    match schedules_form.delete_schedule {
        Some(schedule_id) => Ok(
            match Schedule::delete_schedule(&cookies, schedule_id.parse().unwrap()).await? {
                Some(c) => (
                    CookieJar::new().add(c),
                    Redirect::to("temperature_schedules"),
                )
                    .into_response(),
                None => Redirect::to(Urls::get("temperature_schedules")).into_response(),
            },
        ),
        None => {
            let tc = Urls::get("temperature_schedules");
            Ok(
                match Schedule::create_schedule(&cookies, schedules_form.schedule_name.unwrap())
                    .await?
                {
                    Some(c) => (CookieJar::new().add(c), Redirect::to(tc)).into_response(),
                    None => Redirect::to(tc).into_response(),
                },
            )
        }
    }
}
