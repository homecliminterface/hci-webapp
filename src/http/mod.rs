pub mod askama_axum;
mod static_files;
pub mod templates;
pub mod urls;

use self::{
    static_files::file_handler,
    templates::{
        default_temperatures::{default_temperatures_get, default_temperatures_post},
        heater_valves::{heater_valves_get, heater_valves_post},
        login::{login_get, login_post},
        logout::{logout_get, logout_post},
        parameters::{parameters_get, parameters_post},
        schedule_detail::{temperature_schedule_detail_get, temperature_schedule_detail_post},
        temperature_schedules::{temperature_schedules_get, temperature_schedules_post},
    },
    urls::Urls,
};
use axum::{
    Router,
    routing::{get, post},
};
use templates::heater_valves::holiday_post;
use tokio::net::TcpListener;

#[derive(Clone)]
pub struct ServerConfig {
    pub host: String,
    pub port: String,
}

pub async fn run_server(host: &str, port: &str) {
    let server_config = ServerConfig {
        host: host.into(),
        port: port.into(),
    };

    let address = format!("{}:{}", host, port);
    let listener = TcpListener::bind(address).await.unwrap();

    let static_routes = Router::new().route("/{id}", get(file_handler));

    let app = Router::new()
        .nest(Urls::get("static"), static_routes)
        .route(
            Urls::get("heater_valves"),
            get(heater_valves_get).post(heater_valves_post),
        )
        .route(
            Urls::get("temperature_schedules"),
            get(temperature_schedules_get).post(temperature_schedules_post),
        )
        .route(
            &format!("{}/{{id}}", Urls::get("temperature_schedules")),
            get(temperature_schedule_detail_get).post(temperature_schedule_detail_post),
        )
        .route(Urls::get("login"), get(login_get).post(login_post))
        .route(Urls::get("logout"), get(logout_get).post(logout_post))
        .route(
            Urls::get("preset_temperatures"),
            get(default_temperatures_get).post(default_temperatures_post),
        )
        .route(
            Urls::get("parameters"),
            get(parameters_get).post(parameters_post),
        )
        .route(Urls::get("holiday"), post(holiday_post))
        .with_state(server_config);

    axum::serve(listener, app.into_make_service())
        .await
        .unwrap();
}
